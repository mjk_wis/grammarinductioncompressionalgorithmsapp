package com.wisniewski;

import com.wisniewski.algorithms.genetic.GeneticAlgorithm;
import com.wisniewski.algorithms.genetic.Individual;
import com.wisniewski.algorithms.genetic.InputString;
import com.wisniewski.algorithms.genetic.Population;
import com.wisniewski.algorithms.li_chen.LiChen;
import com.wisniewski.algorithms.sequitur.Sequitur;
import com.wisniewski.algorithms.sequitur.util.NamingOrder;
import com.wisniewski.algorithms.sequitur.util.NamingStrategy;
import com.wisniewski.algorithms.sequitur.util.RulePrinter;
import com.wisniewski.algorithms.staniewicz.Staniewicz;
import com.wisniewski.algorithms.wisniewski.Wisniewski;
import com.wisniewski.sequence.generator.SequenceGenerator;

import java.util.ArrayList;
import java.util.List;

public class AllInOne {
    private static final int INPUT_LENGTH = 100;
    private static final int NO_OF_SYMBOLS = 10;
    private static final int MAX_GENERATIONS = 60;
    private static final int OFFSET = 2;
    private static final boolean LETTERS = false;

//        String input = "gaattctctgtaacactaagctctcttcctcaaaaccagaggtagatagaatgtgtaataatttacagaatttctagacttcaacgatctgattttttaaatttatttttattttttcaggttgagactgagctaaagttaatctgtggc";

    public static void main(String[] args) {
        List<Integer> chromosome = new ArrayList<>();
        SequenceGenerator sequenceGenerator = new SequenceGenerator(INPUT_LENGTH, NO_OF_SYMBOLS, LETTERS);
        String input = sequenceGenerator.generateSequence();

        System.out.println(String.format("Input: %s", input));
        System.out.println(String.format("Input length: %s", input.length()));
        InputString inputString = new InputString(input);

        GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm();
        Population population = geneticAlgorithm.initPopulation(input.length());
        geneticAlgorithm.evalPopulation(population, inputString);
        Individual fittest = null;

        for (int i = 0; i < MAX_GENERATIONS; i++) {
            fittest = population.getFittest(OFFSET);
            population = geneticAlgorithm.crossoverPopulation(population);
            population = geneticAlgorithm.mutatePopulation(population);
            geneticAlgorithm.evalPopulation(population, inputString);
            System.out.println(String.format("Generation %d -> Best solution (%f) -> %s", i + 1, fittest.getFitness(), fittest.toString()));
        }

        int[] chromosomeAsIntTable = fittest.getChromosome();

        for (int elem : chromosomeAsIntTable) {
            chromosome.add(elem);
        }

        System.out.println("\n***************ALGORITHMS***************");
        System.out.println("\n***************Sequitur***************");
        Sequitur sequitur = new Sequitur();
        sequitur.process(input);
        RulePrinter printer = new RulePrinter(sequitur,
                NamingStrategy.USE_LETTERS_START_WITH_S,
                NamingOrder.BY_CREATION);
        System.out.println(printer.getTable());

        System.out.println("\n***************Li-Chen***************");
        LiChen liChenAlgorithm = new LiChen(chromosome);
        liChenAlgorithm.process(input, true);

        System.out.println("\n***************Staniewicz***************");
        Staniewicz staniewiczAlgorithm = new Staniewicz(chromosome);
        staniewiczAlgorithm.process(input, true);

        System.out.println("\n***************Wiśniewski***************");
        Wisniewski wisniewskiAlgorithm = new Wisniewski(chromosome);
        wisniewskiAlgorithm.process(input, true);
    }
}
