package com.wisniewski.algorithms.wisniewski;


public class Rule {

    private int count;

    private int number;

    private String letter;

    private Digram digram;

    private String finalElements;

    public Rule() {
    }

    Rule(Wisniewski wisniewski, Digram digram) {
        this.digram = digram;
        this.finalElements = digram.toString();
        this.letter = wisniewski.getNextLetter(wisniewski.getNumRules());
        wisniewski.onRuleCreation();
        this.number = wisniewski.getNumRules();
    }

    public int getNumber() {
        return number;
    }

    public Digram getDigram() {
        return digram;
    }

    public String getLetter() {
        return letter;
    }

    public void increaseCount() {
        this.count++;
    }

    public int getCount() {
        return count;
    }

    public String getFinalElements() {
        return finalElements;
    }

    public void setFinalElements(String finalElements) {
        this.finalElements = finalElements;
    }

    public void eraseCount() {
        this.count = 0;
    }
}
