/*
package com.wisniewski.algorithms.wisniewski;

import com.wisniewski.algorithms.genetic.GeneticAlgorithm;
import com.wisniewski.algorithms.genetic.Individual;
import com.wisniewski.algorithms.genetic.InputString;
import com.wisniewski.algorithms.genetic.Population;
import com.wisniewski.sequence.generator.SequenceGenerator;

import java.util.ArrayList;
import java.util.List;

//FOR NOW THE SAME AS LICHEN

public class Main {
    public static final int MAX_GENERATIONS = 40;
    public static final int OFFSET = 1;
    private static final int INPUT_LENGTH = 150;
    private static final int NO_OF_SYMBOLS = 4;

    public static void main(String[] args) {

        List<Integer> chromosome = new ArrayList<>();

//        if (args.length < 1) {
//            System.out.println(
//                    "usage: " + com.wisniewski.algorithms.sequitur.Main.class.getSimpleName() + " <input>");
//            System.exit(1);
//        }
//
//        SequenceGenerator sequenceGenerator = new SequenceGenerator(INPUT_LENGTH);
//        String input = sequenceGenerator.nextString(INPUT_LENGTH, NO_OF_SYMBOLS);

        InputString inputString = new InputString(input);
        System.out.println("***************INPUT***************");
        System.out.println(String.format("Input: %s -> length: %d", input, input.length()));


        GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm();
        Population population = geneticAlgorithm.initPopulation(input.length());
        geneticAlgorithm.evalPopulation(population, inputString);
        Individual fittest = null;

        for (int i = 0; i < MAX_GENERATIONS; i++) {
            // Print fittest individual from population
            fittest = population.getFittest(OFFSET);
            System.out.println(String.format("Generation %d -> Best solution (%f) -> %s", i + 1, fittest.getFitness(), fittest.toString()));
            population = geneticAlgorithm.crossoverPopulation(population);
            population = geneticAlgorithm.mutatePopulation(population);
            // Evaluate population
            geneticAlgorithm.evalPopulation(population, inputString);
        }


        int[] chromosomeAsIntTable = fittest.getChromosome();

        for (int elem : chromosomeAsIntTable) {
            chromosome.add(elem);
        }

        System.out.println("***************CHROMOSOME***************");
        System.out.println(chromosome.toString());

        System.out.println("\n***************ALGORITHM***************");
        Wisniewski wisniewskiAlgorithm = new Wisniewski(chromosome);
        wisniewskiAlgorithm.process(input, true);
    }
}
*/
