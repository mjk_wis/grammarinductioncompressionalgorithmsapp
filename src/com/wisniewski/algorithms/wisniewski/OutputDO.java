package com.wisniewski.algorithms.wisniewski;

import java.util.List;

public class OutputDO {

    private String output;
    private List<Rule> rules;

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }
}
