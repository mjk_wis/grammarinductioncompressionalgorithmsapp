package com.wisniewski.algorithms.genetic;

import com.wisniewski.algorithms.li_chen.LiChen;
import com.wisniewski.algorithms.li_chen.OutputDO;

import java.util.ArrayList;
import java.util.List;

public class GeneticAlgorithm {

    public static final int ELITISM_COUNT = 2;
    public static final int TOURNAMENT_SIZE = 6;
    private static final int POPULATION_SIZE = 35;
    private static final double MUTATION_RATE = 0.01;
    private static final double CROSSOVER_RATE = 0.1;

    public GeneticAlgorithm() {}

    public Population initPopulation(int chromosomeLength) {
        // Initialize population
        Population population = new Population(POPULATION_SIZE, chromosomeLength);
        return population;
    }

    public double calculateFitnessValue(Individual individual, InputString inputString) {
        int[] chromosomeAsIntTable = individual.getChromosome();
        List<Integer> chromosome = new ArrayList<>();

        for (int elem : chromosomeAsIntTable) {
            chromosome.add(elem);
        }

        int inputLength = inputString.getLength();
        LiChen liChen = new LiChen(chromosome);
        OutputDO outputDO = liChen.process(inputString.getInputLine(), false);
        int outputLength = outputDO.getOutput().length();
        int outputRulesSize = outputDO.getRules().size();
        double fitness = inputLength - outputLength - outputRulesSize;
        individual.setFitness(fitness);
        return fitness;
    }

    public void evalPopulation(Population population, InputString inputString) {
        double populationFitness = 0;

        for (Individual individual : population.getIndividuals()) {
            populationFitness += this.calculateFitnessValue(individual, inputString);
        }

        population.setPopulationFitness(populationFitness);
    }

    public Individual selectParent(Population population) {
        Population tournament = new Population(TOURNAMENT_SIZE);

        population.shuffle();
        for (int i = 0; i < TOURNAMENT_SIZE; i++) {
            Individual tournamentIndividual = population.getIndividual(i);
            tournament.setIndividual(i, tournamentIndividual);
        }

        return tournament.getFittest(0);
    }

    public Population mutatePopulation(Population population) {
        Population newPopulation = new Population(POPULATION_SIZE);

        for (int populationIndex = 0; populationIndex < population.size(); populationIndex++) {
            Individual individual = population.getFittest(populationIndex);

            for (int geneIndex = 0; geneIndex < individual.getChromosomeLength(); geneIndex++) {
                if (populationIndex >= ELITISM_COUNT) {
                    if (this.MUTATION_RATE > Math.random()) {
                        int newGene = 1;
                        if (individual.getGene(geneIndex) == 1) {
                            newGene = 0;
                        }
                        individual.setGene(geneIndex, newGene);
                    }
                }
            }

            newPopulation.setIndividual(populationIndex, individual);
        }
        return newPopulation;
    }

    public Population crossoverPopulation(Population population) {
        Population newPopulation = new Population(population.size());

        for (int populationIndex = 0; populationIndex < population.size(); populationIndex++) {
            Individual parent1 = population.getFittest(populationIndex);

            if (CROSSOVER_RATE > Math.random() && populationIndex >= CROSSOVER_RATE) {
                Individual offspring = new Individual(parent1.getChromosomeLength());

                Individual parent2 = this.selectParent(population);

                int crossoverPoint = (int) (Math.random() * (parent1.getChromosomeLength() + 1));

                for (int geneIndex = 0; geneIndex < parent1.getChromosomeLength(); geneIndex++) {
                    if (geneIndex < crossoverPoint) {
                        offspring.setGene(geneIndex, parent1.getGene(geneIndex));
                    } else {
                        offspring.setGene(geneIndex, parent2.getGene(geneIndex));
                    }
                }

                newPopulation.setIndividual(populationIndex, offspring);
            } else {
                newPopulation.setIndividual(populationIndex, parent1);
            }
        }

        return newPopulation;
    }
}

