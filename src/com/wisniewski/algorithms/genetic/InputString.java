package com.wisniewski.algorithms.genetic;

public class InputString {
    private String inputLine;
    private int length;

    public InputString(String inputLine) {
        this.inputLine = inputLine;
        this.length = inputLine.length();
    }

    public String getInputLine() {
        return inputLine;
    }

    public void setInputLine(String inputLine) {
        this.inputLine = inputLine;
    }

    public int getLength() {
        return length;
    }
}
