package com.wisniewski.algorithms.genetic;

import java.util.List;

public class Main {
    public static final int MAX_GENERATIONS = 1000;
    public static final int OFFSET = 0;

    public static void main(String[] args) {
   /*     InputString inputString = new InputString(INPUT_STRING);

        GeneticAlgorithm ga = new GeneticAlgorithm();
        Population population = ga.initPopulation(INPUT_STRING.length());
        ga.evalPopulation(population, inputString);

        for (int i = 0; i < MAX_GENERATIONS; i++) {
            // Print fittest individual from population
            Individual fittest = population.getFittest(OFFSET);
            System.out.println(String.format("Generation %d -> Best solution (%f) -> %s", i + 1, fittest.getFitness(), fittest.toString()));

            population = ga.crossoverPopulation(population);

            population = ga.mutatePopulation(population);

            // Evaluate population
            ga.evalPopulation(population, inputString);
        }

        System.out.println("Stopped after " + MAX_GENERATIONS + " generations.");
        Individual fittest = population.getFittest(0);
        System.out.println("Best solution (" + fittest.getFitness() + "): " + fittest.toString());

        return new ArrayList<>();
    }

    */
    }
}

