package com.wisniewski.algorithms.sequitur;

public class Rule {
    public Guard guard;

    public int count;

    public int number;

    Rule(Sequitur sequitur) {
        number = sequitur.getNextRuleId();
        guard = new Guard(sequitur, this);
        count = 0;
    }

    public Symbol first() {
        return guard.n;
    }

    public Symbol last() {
        return guard.p;
    }

}
