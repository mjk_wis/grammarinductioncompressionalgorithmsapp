package com.wisniewski.algorithms.sequitur;

import com.wisniewski.algorithms.sequitur.util.NamingOrder;
import com.wisniewski.algorithms.sequitur.util.NamingStrategy;
import com.wisniewski.algorithms.sequitur.util.RulePrinter;
import com.wisniewski.sequence.generator.SequenceGenerator;

public class Main {
    private static final int INPUT_LENGTH = 150;
    private static final int NO_OF_SYMBOLS = 4;

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println(
                    "usage: " + Main.class.getSimpleName() + " <input>");
            System.exit(1);
        }

        String input = args[0];
//        SequenceGenerator sequenceGenerator = new SequenceGenerator(INPUT_LENGTH);
//        String input = "11112131131";

        Sequitur sequitur = new Sequitur();
        sequitur.process(input);
        RulePrinter printer = new RulePrinter(sequitur,
                NamingStrategy.USE_LETTERS_START_WITH_S,
                NamingOrder.BY_CREATION);
        System.out.println(printer.getTable());
    }
}
