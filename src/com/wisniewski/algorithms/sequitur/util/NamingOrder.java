package com.wisniewski.algorithms.sequitur.util;

public enum NamingOrder {

    BY_CREATION, //
    BY_USAGE

}
