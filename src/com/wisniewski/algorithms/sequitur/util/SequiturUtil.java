package com.wisniewski.algorithms.sequitur.util;

import com.wisniewski.algorithms.sequitur.*;

public class SequiturUtil {

    public static Rule run(String input) {
        Sequitur sequitur = new Sequitur();
        sequitur.process(input);
        return sequitur.getFirstRule();
    }

}
