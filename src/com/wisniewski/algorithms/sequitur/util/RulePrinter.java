package com.wisniewski.algorithms.sequitur.util;

import java.util.*;
import com.wisniewski.algorithms.sequitur.*;

public class RulePrinter
{

    private Sequitur sequitur;

    private StringBuilder text;

    public StringBuilder getText() {
        return text;
    }

    private List<Rule> rules;

    public List<Rule> getRules() {
        return rules;
    }

    private Map<Rule, Integer> ruleToIndex = new HashMap<>();

    private NamingStrategy strategy;
    private NamingOrder order;

    public RulePrinter(Sequitur sequitur, NamingStrategy strategy,
                       NamingOrder order)
    {
        this(sequitur, strategy, order, "Usage\tRule\n");
    }

    public RulePrinter(Sequitur sequitur, NamingStrategy strategy,
                       NamingOrder order, String header)
    {
        this.sequitur = sequitur;
        this.strategy = strategy;
        this.order = order;
        rules = new ArrayList<>(sequitur.getNumRules());
        int processedRules = 0;
        text = new StringBuilder();

        text.append(header);
        rules.add(sequitur.getFirstRule());
        ruleToIndex.put(sequitur.getFirstRule(), 0);
        while (processedRules < rules.size()) {
            Rule currentRule = rules.get(processedRules);
            traverse(currentRule, processedRules);
            processedRules++;
        }

        if (order == NamingOrder.BY_CREATION) {
            Collections.sort(rules, new Comparator<Rule>() {
                @Override
                public int compare(Rule r1, Rule r2)
                {
                    return r1.number - r2.number;
                }
            });
        }

        for (int i = 0; i < rules.size() - 1; i++) {
            Rule rule = rules.get(i);
            appendRow(text, rule);
            text.append('\n');
        }
        appendRow(text, rules.get(rules.size() - 1));
    }

    private void traverse(Rule currentRule, int processedRules)
    {
        for (Symbol sym = currentRule.first(); (!sym.isGuard()); sym = sym
                .getNext()) {
            if (sym.isNonTerminal()) {
                Rule referedTo = ((NonTerminal) sym).getRule();
                Integer indexOrNull = ruleToIndex.get(referedTo);
                int index = indexOrNull == null ? 0 : indexOrNull;
                if ((rules.size() <= index)
                        || (rules.get(index) != referedTo)) {
                    ruleToIndex.put(referedTo, rules.size());
                    rules.add(referedTo);
                }
            }
        }
    }

    public String getTable()
    {
        return text.toString();
    }

    public String getName(Rule rule)
    {
        int index;
        switch (order) {
            default:
            case BY_CREATION:
                index = rule.number;
                break;
            case BY_USAGE:
                index = ruleToIndex.get(rule);
                break;
        }
        switch (strategy) {
            default:
            case USE_NUMBER:
                return "" + index;
            case USE_R_PLUS_NUMBER:
                return "R" + index;
            case USE_LETTERS: {
                int offset = index % 26;
                char c = (char) ('A' + offset);
                if (index < 26) {
                    return "" + c;
                }
                return c + "" + (index / 26);
            }
            case USE_LETTERS_START_WITH_S: {
                if (index == 0) {
                    return "S";
                }
                index -= 1;
                int offset = index % 25;
                char c;
                if (offset < 18) {
                    c = (char) ('A' + offset);
                } else {
                    c = (char) ('A' + offset + 1);
                }
                if (index < 25) {
                    return "" + c;
                }
                return c + "" + (index / 25);
            }
        }
    }

    public String getRightSide(Rule rule)
    {
        StringBuilder text = new StringBuilder();
        createRightSide(text, rule);
        return text.toString();
    }

    public String getProduction(Rule rule)
    {
        return getName(rule) + " -> " + getRightSide(rule);
    }

    public String getSymbol(Symbol symbol)
    {
        StringBuilder text = new StringBuilder();
        appendSymbol(text, symbol);
        return text.toString();
    }

    private void appendRow(StringBuilder text, Rule currentRule)
    {
        text.append(" ");
        text.append(currentRule.count);
        text.append("\t");
        text.append(getName(currentRule));
        text.append(" -> ");
        createRightSide(text, currentRule);
    }

    private void createRightSide(StringBuilder text, Rule rule)
    {
        for (Symbol sym = rule.first(); (!sym.isGuard()); sym = sym.getNext()) {
            appendSymbol(text, sym);
            text.append(' ');
        }
    }

    private void appendSymbol(StringBuilder text, Symbol symbol)
    {
        if (symbol.isNonTerminal()) {
            Rule referedTo = ((NonTerminal) symbol).getRule();
            text.append(getName(referedTo));
        } else if (symbol.isGuard()) {
            text.append("$guard$");
        } else {
            int value = symbol.getValue();
            if (value == ' ') {
                text.append('_');
            } else {
                if (value == '\n') {
                    text.append("\\n");
                } else {
                    text.append((char) value);
                }
            }
        }
    }

}
