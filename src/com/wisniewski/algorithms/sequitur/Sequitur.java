package com.wisniewski.algorithms.sequitur;

import java.util.Hashtable;

public class Sequitur {

    private int numRules = 0;

    private Hashtable<Symbol, Symbol> digrams = new Hashtable<>(Symbol.prime);

    private Rule firstRule = new Rule(this);

    private DebugCallback debugCallback;

    public Sequitur() {
        this(null);
    }

    public Sequitur(DebugCallback debugCallback) {
        this.debugCallback = debugCallback;
    }

    public void setDebugCallback(DebugCallback debugCallback) {
        this.debugCallback = debugCallback;
    }

    public void process(char c) {
        firstRule.last().insertAfter(new Terminal(this, c));
        boolean check = firstRule.last().p.check();
    }

    public void process(String input) {
        for (int i = 0; i < input.length(); i++) {
            process(input.charAt(i));
        }
    }

    public void join(Symbol left, Symbol right) {
        if (left.n != null) {
            left.deleteDigram();

            if ((right.p != null) && (right.n != null)
                    && right.value == right.p.value
                    && right.value == right.n.value) {
                digrams.put(right, right);
            }

            if ((left.p != null) && (left.n != null)
                    && left.value == left.n.value
                    && left.value == left.p.value) {
                digrams.put(left.p, left.p);
            }
        }

        left.n = right;
        right.p = left;
    }

    public int getNumRules() {
        return numRules;
    }

    public int getNextRuleId() {
        int n = numRules;
        numRules++;
        return n;
    }

    public Hashtable<Symbol, Symbol> getDigrams() {
        return digrams;
    }

    public Rule getFirstRule() {
        return firstRule;
    }

    void emitLookForDigram(Symbol symbol) {
        if (debugCallback != null) {
            debugCallback.lookForDigram(symbol);
        }
    }

    void emitDigramNotFound() {
        if (debugCallback != null) {
            debugCallback.digramNotFound();
        }
    }

    void emitInsertDigram(Symbol symbol) {
        if (debugCallback != null) {
            debugCallback.insertDigram(symbol);
        }
    }

    void emitRemoveDigram(Symbol symbol) {
        if (debugCallback != null) {
            if (!symbol.isGuard() && !symbol.getNext().isGuard()) {
                debugCallback.removeDigram(symbol);
            }
        }
    }

    void emitPreCreateRule(Rule rule) {
        if (debugCallback != null) {
            debugCallback.preCreateRule(rule);
        }
    }

    void emitCreateRule(Rule rule) {
        if (debugCallback != null) {
            debugCallback.createRule(rule);
        }
    }

    void emitPreUnderusedRule(NonTerminal nonTerminal) {
        if (debugCallback != null) {
            debugCallback.preUnderusedRule(nonTerminal);
        }
    }

    void emitUnderusedRule(NonTerminal nonTerminal) {
        if (debugCallback != null) {
            debugCallback.underusedRule(nonTerminal);
        }
    }

    void emitPreReuseRule(Rule rule) {
        if (debugCallback != null) {
            debugCallback.preReuseRule(rule);
        }
    }

    void emitReuseRule(Rule rule) {
        if (debugCallback != null) {
            debugCallback.reuseRule(rule);
        }
    }

    void emitPostCreateRule(Rule rule) {
        if (debugCallback != null) {
            debugCallback.postCreateRule(rule);
        }
    }
}
