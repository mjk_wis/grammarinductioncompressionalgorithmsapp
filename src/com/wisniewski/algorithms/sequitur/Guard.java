package com.wisniewski.algorithms.sequitur;

public class Guard extends Symbol {

    Rule r;

    Guard(Sequitur sequitur, Rule rule) {
        super(sequitur);
        r = rule;
        value = 0;
        p = this;
        n = this;
    }

    @Override
    public void cleanUp() {
        sequitur.join(p, n);
    }

    @Override
    public boolean isGuard() {
        return true;
    }

    @Override
    public void deleteDigram() {
        // Do nothing
    }

    @Override
    public boolean check() {
        return false;
    }
}
