package com.wisniewski.algorithms.sequitur;

public class Terminal extends Symbol implements Cloneable
{

    Terminal(Sequitur sequitur, int value)
    {
        super(sequitur);
        this.value = value;
        p = null;
        n = null;
    }

    @Override
    public void cleanUp()
    {
        sequitur.join(p, n);
        deleteDigram();
    }
}
