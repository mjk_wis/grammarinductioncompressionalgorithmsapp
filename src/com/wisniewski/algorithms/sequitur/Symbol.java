package com.wisniewski.algorithms.sequitur;

public abstract class Symbol {

    static final int numTerminals = 100000;

    static final int prime = 2265539;

    protected Sequitur sequitur;

    int value;
    Symbol p, n;

    public Symbol(Sequitur sequitur) {
        this.sequitur = sequitur;
    }

    public int getValue() {
        return value;
    }

    public Symbol getPrevious() {
        return p;
    }

    public Symbol getNext() {
        return n;
    }

    public abstract void cleanUp();

    public void insertAfter(Symbol toInsert) {
        sequitur.join(toInsert, n);
        sequitur.join(this, toInsert);
    }

    public void deleteDigram() {
        Symbol dummy;

        if (n.isGuard()) {
            return;
        }
        dummy = sequitur.getDigrams().get(this);

        if (dummy == this) {
            sequitur.emitRemoveDigram(this);
            sequitur.getDigrams().remove(this);
        }
    }

    public boolean isGuard() {
        return false;
    }

    public boolean isNonTerminal() {
        return false;
    }

    public boolean check() {
        Symbol found;

        if (n.isGuard()) {
            return false;
        }
        sequitur.emitLookForDigram(this);
        if (!sequitur.getDigrams().containsKey(this)) {
            sequitur.emitInsertDigram(this);
            found = sequitur.getDigrams().put(this, this);
            sequitur.emitDigramNotFound();
            return false;
        }
        found = sequitur.getDigrams().get(this);
        if (found.n != this) {
            match(this, found);
        }
        return true;
    }

    public void substitute(Rule r) {
        cleanUp();
        n.cleanUp();
        p.insertAfter(new NonTerminal(sequitur, r));
    }

    public void checkAfterSubstitute(Rule r) {
        if (!p.check()) {
            p.n.check();
        }
    }

    public void match(Symbol newD, Symbol matching) {
        Rule r;

        if (matching.p.isGuard() && matching.n.n.isGuard()) {
            // reuse an existing rule

            r = ((Guard) matching.p).r;
            sequitur.emitPreReuseRule(r);
            newD.substitute(r);
            sequitur.emitReuseRule(r);
            newD.checkAfterSubstitute(r);
        } else {
            r = new Rule(sequitur);
            try {
                Symbol first = (Symbol) newD.clone();
                Symbol second = (Symbol) newD.n.clone();
                r.guard.n = first;
                first.p = r.guard;
                first.n = second;
                second.p = first;
                second.n = r.guard;
                r.guard.p = second;

                sequitur.emitPreCreateRule(r);
                matching.substitute(r);
                newD.substitute(r);
                sequitur.emitCreateRule(r);
                matching.checkAfterSubstitute(r);
                if (matching.n.n.value != newD.value) {
                    newD.checkAfterSubstitute(r);
                }

                sequitur.emitInsertDigram(first);
                sequitur.getDigrams().put(first, first);
                sequitur.emitPostCreateRule(r);
            } catch (CloneNotSupportedException c) {
                c.printStackTrace();
            }
        }

        if (r.first().isNonTerminal()
                && (((NonTerminal) r.first()).r.count == 1)) {
            ((NonTerminal) r.first()).expand();
        }
    }

    @Override
    public String toString() {
        if (n == null) {
            return "Symbol(" + value + "," + n + ")";
        }
        return "Symbol(" + value + "," + n.value + ")";
    }

    @Override
    public int hashCode() {
        long code;
        if (n == null) {
            code = ((21599 * (long) value));
        } else {
            code = ((21599 * (long) value) + (20507 * (long) n.value));
        }
        code = code % prime;
        return (int) code;
    }

    @Override
    public boolean equals(Object obj) {
        Symbol o = (Symbol) obj;
        if (value != o.value) {
            return false;
        }
        if (n == null && o.n == null) {
            return true;
        }
        if (n != null && o.n != null) {
            return n.value == o.n.value;
        }
        return false;
    }
}
