package com.wisniewski.algorithms.sequitur;

import com.wisniewski.algorithms.sequitur.util.NamingOrder;
import com.wisniewski.algorithms.sequitur.util.NamingStrategy;
import com.wisniewski.algorithms.sequitur.util.RulePrinter;

public class MainWithOutput implements DebugCallback {

    String headline = "********************************************************************************";
    private Sequitur sequitur;

    public MainWithOutput() {
        sequitur = new Sequitur(this);
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("usage: "
                    + MainWithOutput.class.getSimpleName() + " <input>");
            System.exit(1);
        }

        String input = args[0];

        MainWithOutput stepwise = new MainWithOutput();
        stepwise.execute(input);
    }

    private void execute(String input) {
        StringBuilder soFar = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            soFar.append(c);

            RulePrinter printer = createPrinter();
            System.out.println(headline);
            System.out.println(printer.getTable());

            System.out.println(headline);
            System.out.println(
                    "Next character: '" + c + "', processed: '" + soFar + "'");
            System.out.println(headline);
            System.out.println(
                    "* " + printer.getProduction(sequitur.getFirstRule()) + c);

            sequitur.process(c);
        }
    }

    private RulePrinter createPrinter() {
        RulePrinter printer = new RulePrinter(sequitur,
                NamingStrategy.USE_LETTERS_START_WITH_S,
                NamingOrder.BY_CREATION);
        return printer;
    }

    @Override
    public void preReuseRule(Rule rule) {
        RulePrinter printer = createPrinter();
        System.out.println(" > reuse: " + printer.getProduction(rule));
    }

    @Override
    public void reuseRule(Rule rule) {
        RulePrinter printer = createPrinter();
        System.out.println();
        System.out.println(printer.getTable());
        System.out.println();
    }

    @Override
    public void preCreateRule(Rule rule) {
        // ignore
    }

    @Override
    public void createRule(Rule rule) {
        RulePrinter printer = createPrinter();

        System.out.println(
                " > Create production: " + printer.getProduction(rule));
        System.out.println();
        System.out.println(printer.getTable());
        System.out.println();
    }

    @Override
    public void preUnderusedRule(NonTerminal nonTerminal) {
        // ignore
    }

    @Override
    public void underusedRule(NonTerminal nonTerminal) {
        RulePrinter printer = createPrinter();

        System.out.println("* Underused production: "
                + printer.getName(nonTerminal.getRule()));
        System.out.println();

        System.out.println(printer.getTable());
    }

    @Override
    public void lookForDigram(Symbol symbol) {
        RulePrinter printer = createPrinter();
        System.out.println("* look for digram: " + printer.getSymbol(symbol)
                + printer.getSymbol(symbol.getNext()));
    }

    @Override
    public void digramNotFound() {
        System.out.println(" > Digram not found");
    }

    @Override
    public void postCreateRule(Rule rule) {
        // ignore
    }

    @Override
    public void insertDigram(Symbol symbol) {
        // ignore
    }

    @Override
    public void removeDigram(Symbol symbol) {
        // ignore
    }

}

