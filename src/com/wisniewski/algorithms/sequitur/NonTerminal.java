package com.wisniewski.algorithms.sequitur;

public class NonTerminal extends Symbol implements Cloneable {

    Rule r;

    NonTerminal(Sequitur sequitur, Rule rule) {
        super(sequitur);
        r = rule;
        r.count++;
        value = numTerminals + r.number;
        p = null;
        n = null;
    }

    public Rule getRule() {
        return r;
    }

    @Override
    protected Object clone() {
        NonTerminal sym = new NonTerminal(sequitur, r);

        sym.p = p;
        sym.n = n;
        return sym;
    }

    @Override
    public void cleanUp() {
        sequitur.join(p, n);
        deleteDigram();
        r.count--;
    }

    @Override
    public boolean isNonTerminal() {
        return true;
    }

    public void expand() {
        sequitur.emitPreUnderusedRule(this);

        deleteDigram();

        sequitur.join(p, r.first());
        sequitur.join(r.last(), n);

        sequitur.emitInsertDigram(r.last());
        sequitur.getDigrams().put(r.last(), r.last());

        r.guard.r = null;
        r.guard = null;

        sequitur.emitUnderusedRule(this);
    }
}
