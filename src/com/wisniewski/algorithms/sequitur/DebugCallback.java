package com.wisniewski.algorithms.sequitur;

public interface DebugCallback {

    void lookForDigram(Symbol symbol);

    void insertDigram(Symbol symbol);

    void removeDigram(Symbol symbol);

    void digramNotFound();

    void preCreateRule(Rule rule);

    void preReuseRule(Rule rule);

    void reuseRule(Rule rule);

    void createRule(Rule rule);

    void preUnderusedRule(NonTerminal nonTerminal);

    void underusedRule(NonTerminal nonTerminal);

    void postCreateRule(Rule rule);

}
