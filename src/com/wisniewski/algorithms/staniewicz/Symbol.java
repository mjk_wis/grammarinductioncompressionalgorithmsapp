package com.wisniewski.algorithms.staniewicz;

public class Symbol {
//    static final int numTerminals = 100000;
//
//    static final int prime = 2265539;

    private char value;
    private int index;

    public Symbol(char value, int index) {
        this.value = value;
        this.index = index;
    }

    public char getValue() {
        return value;
    }

    public int getIndex() {
        return index;
    }
}
