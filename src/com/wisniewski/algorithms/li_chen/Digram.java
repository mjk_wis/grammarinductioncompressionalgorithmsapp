package com.wisniewski.algorithms.li_chen;

public class Digram {
    private Symbol first;
    private Symbol second;

    public Digram(Symbol first, Symbol second) {
        this.first = first;
        this.second = second;
    }

    public Symbol getFirst() {
        return first;
    }

    public Symbol getSecond() {
        return second;
    }

    @Override
    public String toString() {
        return String.format("%s%s", first.getValue(), second.getValue());
    }
}
