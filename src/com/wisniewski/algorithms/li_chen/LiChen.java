package com.wisniewski.algorithms.li_chen;

import java.util.ArrayList;
import java.util.List;

public class LiChen {

    /*
     * LiChen pseudocode:
     *
     * LiChen(sequence)
     *   GetChromosome(sequence)
     *
     *   DO(sequence)
     *       IF(TRIGRAM.type() == 1 || TRIGRAM.type() == 3)
     *           IF(chromosome == 1)
     *               mergeLeftDigram()
     *           ELSE
     *               IF(TRIGRAM.type() == 3)
     *                   mergeRightDigram()
     *               END IF
     *           END IF
     *       END IF
     *   WHILE(end of the sequence)
     * END
     * */

    private final List<Integer> chromosome;
    private final List<Digram> digrams = new ArrayList<>();
    private final List<Rule> rules = new ArrayList<>();
    private int numRules = 0;
    private int chromosomeIdx = 0;

    private String production = "";

    public LiChen(List<Integer> chromosome) {
        this.chromosome = chromosome;
    }

    public void onRuleCreation() {
        this.numRules++;
    }

    public int getNumRules() {
        return numRules;
    }

    public OutputDO process(String input, boolean display) {

        boolean wasSwap = false;
        int trigramType;
        String afterMerge;
        String swappedInput = input;
        StringBuilder soFar = new StringBuilder();
        OutputDO outputDO = new OutputDO();
        this.production = "";
        int tempIdx = 0;

        do {
            if (wasSwap) {
                if (display) {
//                    System.out.println("\nNext iteration after swap\n");
                }
                wasSwap = false;
                for (Rule rule : rules) {
                    addDigramIfNotDuplicate(rule.getDigram());
                }
            }
            soFar.setLength(0);
            for (int i = 2; i < swappedInput.length(); i++) {
                trigramType = 0;

                Symbol first = new Symbol(swappedInput.charAt(i - 2), i - 2);
                Symbol second = new Symbol(swappedInput.charAt(i - 1), i - 1);
                Symbol third = new Symbol(swappedInput.charAt(i), i);

                if (i == 2) {
                    soFar.append(swappedInput.charAt(i - 2));
                    soFar.append(swappedInput.charAt(i - 1));
                }

                soFar.append(swappedInput.charAt(i));
                if (display) {
//                    System.out.println(String.format("So far processed: %s", soFar.toString()));
                }

                Digram leftDigram = new Digram(first, second);
                Digram rigthDigram = new Digram(second, third);
                Rule rule;

                if (i > 3 && i > tempIdx) {
                    //check if trigram type 1 or 3
                    if (checkIfDigramExist(leftDigram)) { // at least trigram type 1
                        if (checkIfDigramExist(rigthDigram)) {
                            trigramType = 3;
                        } else {
                            trigramType = 1;
                        }
                    } // at this point we know what is the type of the trigram.
                    if ((trigramType == 1 || trigramType == 3) && chromosome.get(chromosomeIdx) == 1) {
                        if (!checkIfRuleAlreadyExist(leftDigram)) {
                            rule = createRule(leftDigram, display);
                        } else {
                            rule = setUpAlreadyExistingRule(leftDigram);
                        }
                        afterMerge = updateProduction(soFar.toString(), rule, display);
                        tempIdx = afterMerge.length();
                        String toSwap = swappedInput.substring(0, soFar.length());
                        String notToSwap = swappedInput.substring(soFar.length());
                        toSwap = toSwap.replace(soFar, afterMerge);
                        swappedInput = toSwap + notToSwap;
                        wasSwap = true;
                        break;
                    } else if (trigramType == 3 && chromosome.get(chromosomeIdx) == 0) {
                        if (!checkIfRuleAlreadyExist(rigthDigram)) {
                            rule = createRule(rigthDigram, display);
                        } else {
                            rule = setUpAlreadyExistingRule(rigthDigram);
                        }
                        afterMerge = updateProduction(soFar.toString(), rule, display);
                        tempIdx = afterMerge.length();
                        String toSwap = swappedInput.substring(0, soFar.length());
                        String notToSwap = swappedInput.substring(soFar.length());
                        toSwap = toSwap.replace(soFar, afterMerge);
                        swappedInput = toSwap + notToSwap;
                        wasSwap = true;
                        break;
                    } else if (trigramType == 1 && chromosome.get(chromosomeIdx) == 0){
                        tempIdx = soFar.length();
                        this.chromosomeIdx++;
                    }
                }

                addDigramIfNotDuplicate(leftDigram);
            }
        } while (wasSwap);

        this.production = soFar.toString();
        outputDO.setRules(this.rules);
        outputDO.setOutput(this.production);
        if (display) {
            displayProduction();
        }
        return outputDO;
    }

    private Rule setUpAlreadyExistingRule(Digram digram) {
        Rule rule = new Rule();
        this.chromosomeIdx++;
        for (Rule temp : rules) {
            if (temp.getDigram().toString().equals(digram.toString())) {
                rule = temp;
                break;
            }
        }
        return rule;
    }

    private boolean checkIfRuleAlreadyExist(Digram digram) {
        for (Rule rule : rules) {
            if (rule.getDigram().toString().equals(digram.toString())) {
                return true;
            }
        }
        return false;
    }

    private String updateProduction(String soFar, Rule rule, boolean display) {
        if (display) {
//            System.out.println(String.format("Production before update: %s of rule %s", soFar, rule.getLetter()));
        }

        soFar = soFar.replace(rule.getDigram().toString(),rule.getLetter());
        countRuleOccurences(soFar, rule);
        if (display) {
//            System.out.println(String.format("Production after update: %s of rule %s", soFar, rule.getLetter()));
//            System.out.println(String.format("Rule %s containg digram %s was used %d times", rule.getLetter(), rule.getDigram(), rule.getCount()));
        }
        return soFar;
    }

    private void countRuleOccurences(String soFar, Rule rule) {
        // 1. How many in production explicite
        // 2. How many in overrules
        rule.eraseCount();

        // 1
        for (int i = 0; i < soFar.length(); i++) {
            if (Character.toString(soFar.charAt(i)).equals(rule.getLetter())){
                rule.increaseCount();
            }
        }

        // 2
        for (Rule tempRule : this.rules) {
            if (tempRule.getDigram().toString().contains(rule.getLetter())) {
                rule.increaseCount();
            }
        }
    }

    private Rule createRule(Digram digram, boolean display) {
        Rule rule = new Rule(this, digram);
        this.chromosomeIdx++;
        if (display) {
//            System.out.println(String.format("Created rule %s of digram %s", rule.getLetter(), rule.getDigram().toString()));
        }
        rules.add(rule);

        return rule;
    }

    private void addDigramIfNotDuplicate(Digram digram) {
        boolean digramExist = checkIfDigramExist(digram);

        if (!digramExist) {
            digrams.add(digram);
        }
    }

    private boolean checkIfDigramExist(Digram digram) {
        boolean digramAppearsSoFar = false;

        for (int j = 0; j < digrams.size(); j++) {
            if (digrams.get(j).toString().equals(digram.toString())) {
                digramAppearsSoFar = true;
                break;
            }
        }
        return digramAppearsSoFar;
    }

    public String getNextLetter(int ruleIndex) {
        char c = 0;
        String nextRuleLetter;
        if (ruleIndex < 27) {
            c = (char) ('A' + ruleIndex);
        } else if (27 < ruleIndex && ruleIndex < 42){
            c = (char) ('!' + ruleIndex);
        } else {
            c = (char) ('{' + ruleIndex);
        }
        nextRuleLetter = Character.toString(c);
        return nextRuleLetter;
    }

    private void displayProduction() {
        System.out.println("\nUSAGE:\tRULE:");
        System.out.println(String.format("0\tS ->\t%s", this.production));
        for (Rule rule : rules) {
            System.out.println(String.format("%s\t%s ->\t%s", rule.getCount(), rule.getLetter(), rule.getDigram().toString()));
        }
        System.out.println(String.format("\nOutput length %d", this.production.length()));
        System.out.println(String.format("Number of rules %d", this.rules.size()));
    }
}
