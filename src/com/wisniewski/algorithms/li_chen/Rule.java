package com.wisniewski.algorithms.li_chen;


public class Rule {

    private int count;

    private int number;

    private String letter;

    private Digram digram;

    public Rule() {
    }

    Rule(LiChen liChen, Digram digram) {
        this.digram = digram;
        this.letter = liChen.getNextLetter(liChen.getNumRules());
        liChen.onRuleCreation();
        this.number = liChen.getNumRules();
    }

    public int getNumber() {
        return number;
    }

    public Digram getDigram() {
        return digram;
    }

    public String getLetter() {
        return letter;
    }

    public void increaseCount() {
        this.count++;
    }

    public int getCount() {
        return count;
    }

    public void eraseCount() {
        this.count = 0;
    }
}