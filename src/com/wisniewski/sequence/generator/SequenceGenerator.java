package com.wisniewski.sequence.generator;

import java.util.Random;

public class SequenceGenerator {

    private final int howLongSequence;

    private final int howManyTypesOfChars;

    private final boolean letters;

    private char[] possibleChars;

    public SequenceGenerator(int howLongSequence, int howManyTypesOfChars, boolean letters) {
        this.howLongSequence = howLongSequence;
        this.howManyTypesOfChars = howManyTypesOfChars;
        this.letters = letters;
    }

    public String generateSequence() {
        StringBuilder stringBuilder = new StringBuilder();

        possibleChars = new char[this.howManyTypesOfChars];

        if (this.letters) {
            for (int i = 0; i < possibleChars.length; i++) {
                int offset = i % 26;
                char c = (char) ('a' + offset);
                possibleChars[i] = c;
            }
        } else {
            for (int i = 0; i < possibleChars.length; i++) {
                int offset = i % 26;
                char c = (char) ('1' + offset);
                possibleChars[i] = c;
            }
        }

        for (int i = 0; i < this.howLongSequence; i++) {
            Random random = new Random();

            int idx = random.nextInt(this.possibleChars.length);
            char c = this.possibleChars[idx];

            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }
}
